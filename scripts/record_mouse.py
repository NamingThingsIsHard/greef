"""
Options:

https://stackoverflow.com/questions/12384772/how-can-i-capture-mouseevents-and-keyevents-using-python-in-background-on-linux#

https://github.com/JeffHoogland/pyxhook
"""
import os
import signal
from collections import namedtuple
from datetime import datetime
from select import select

import pyautogui
from evdev import InputDevice, list_devices, categorize, KeyEvent
from evdev import ecodes

__author__ = "LoveIsGrief"

MOUSE_NAME = "ImExPS/2 Generic Explorer Mouse"
ButtonAction = namedtuple("ButtonAction", ["position", "keystate", "button"])
# Button names for pyautogui
MOUSE_BUTTONS = {
    "BTN_LEFT": "left",
    "BTN_RIGHT": "right",
    "BTN_MIDDLE": "middle",
}

if os.getuid() != 0:
    # Take care to flush otherwise piping won't work <_<
    print("Must execute as root in order to access /dev/input", flush=True)
    exit(1)

# Script should be runnable without modifying the PYTHONPATH externally
try:
    import actions
except:
    import sys
    sys.path.append(
        os.path.join(os.path.dirname(__file__), "..", "src")
    )
from actions import Click, Drag, Scroll, Action

mouse_dev = next(InputDevice(input_device)
                 for input_device in list_devices()
                 if InputDevice(input_device).name == MOUSE_NAME)

last_action = None  # type: ButtonAction


def classify_button_action(action):
    """
    Creates a MouseAction out of a ButtonAction depending on the last ButtonAction

    :param action:
    :type action: ButtonAction
    :return:
    :rtype: Click | Drag | None
    """
    global last_action  # type: ButtonAction
    ret = None
    if not last_action:
        last_action = action
    # not the same action
    elif action.button != last_action.button:
        ret = Click(last_action.position, MOUSE_BUTTONS[last_action.button])
        last_action = action
    elif last_action.keystate == KeyEvent.key_down and action.keystate == KeyEvent.key_up:
        button = MOUSE_BUTTONS[action.button]
        if action.position == last_action.position:
            # click
            ret = Click(action.position, button)
        else:
            # drag
            ret = Drag(last_action.position, action.position, button)
    elif action:
        last_action = action
    # reset the last action
    if ret:
        last_action = None
    return ret


done = False


# noinspection PyPep8Naming
def handle_SIGTERM():
    global done
    done = True
    mouse_dev.close()


signal.signal(signal.SIGTERM, handle_SIGTERM)

print("from actions import *")
try:
    last_mouse_action = None  # type: tuple[datetime,Action]
    while not done:
        r, w, x = select([mouse_dev], [], [])
        for event in mouse_dev.read():
            # Ignore anything not a mouse click
            input_event = categorize(event)

            # Handle scrolls
            if event.type == ecodes.EV_REL and ecodes.REL[input_event.event.code] == "REL_WHEEL":
                last_action = None
                last_mouse_action = None
                print("%s.execute()" % Scroll(input_event.event.value), flush=True)

            # Handle mouse ev_button presses
            elif event.type == ecodes.EV_KEY:
                if isinstance(input_event.keycode, list):
                    ev_button = input_event.keycode[0]
                else:
                    ev_button = input_event.keycode
                mouse_action = classify_button_action(
                    ButtonAction(pyautogui.position(), input_event.keystate, ev_button)
                )
                if mouse_action:

                    # Calculate how long it took to do the action
                    now = datetime.now()
                    delay = 0
                    if last_mouse_action:
                        delay = (now - last_mouse_action[0]).total_seconds()
                    last_mouse_action = (now, mouse_action)
                    print("%(action)s.execute(%(delay)s)" % {
                        "action": repr(mouse_action),
                        "delay": delay
                    }, flush=True)
except KeyboardInterrupt:
    pass

print("# Terminated purposefully")
