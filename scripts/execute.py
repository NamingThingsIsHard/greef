#!/usr/bin/env python3
import os
import subprocess
from tree import get_paths_until, get_paths_from, traverse_paths


def execute_leaf_path(abs_root, leaf_dirs, record_name):
    """
    Executes the .rec.py scripts in the directory and generates:

        - a video file
        - possibly a playlist if we reach the end of a chain of recordings
          a chain of recordings constitutes of successive .rec.py files with the same name

    A directory can contain:
        - multiple recordings
        - TODO: pre and post execution scripts e.g add an overlay to the video afterwards

    :param abs_root: system path
    :type abs_root:
    :return:
    :rtype:
    """
    for dir_path in traverse_paths(abs_root, leaf_dirs):
        # TODO capture screen as video
        record_path = os.path.join(dir_path, record_name)
        print("running %s" % record_path)
        completed_process = subprocess.run([
            "python3", record_name,
        ], cwd=dir_path, check=True)
        print("\tDone")
        # TODO stop screen capture


def execute_leaf_paths(abs_root, paths, record_name):
    for path in paths:
        execute_leaf_path(abs_root, path, record_name)
        # TODO reset after each execution to start with a clean start


def main(args):
    record_path = os.path.normpath(os.path.abspath(args.recording))
    if args.root:
        execute_leaf_paths(*(get_paths_from(record_path)))
    else:
        execute_leaf_paths(*(get_paths_until(record_path)))


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(
        description="Execute recordings to show features"
    )

    parser.add_argument("-r", "--root", action="store_true",
                        help="Execute all recordings in the tree below the given one")
    parser.add_argument("recording", help="The recording to execute")

    main(parser.parse_args())
