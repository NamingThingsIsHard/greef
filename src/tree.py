"""

"""
from typing import List

__author__ = "LoveIsGrief"

import os
from os import path

RECORDING_EXT = ".rec.py"


def get_recordings(directory):
    dir_contents = os.listdir(directory)
    return [dir_item for dir_item in dir_contents if
            path.isfile(path.join(directory, dir_item)) and dir_item.endswith(RECORDING_EXT)]


def traverse_paths(abs_root, dirs):
    last_dir = abs_root
    dirs = dirs or [""]
    for _dir in dirs:
        curr_dir = path.join(last_dir, _dir)
        yield curr_dir
        last_dir = curr_dir


def paths_have_file(abs_root, dirs, filename):
    for _dir in traverse_paths(abs_root, dirs):
        curr_file = path.join(_dir, filename)
        if not path.isfile(curr_file):
            raise ValueError(curr_file)

    return True


def get_paths_from(record_path):
    """
    Build a list of leaves

    A path to a leaf is made up of directories that contain a recording
     with the same name as the leaf.


    :param record_path:
    :type record_path: basestring
    :return: root_dir, dirs, recordname
    :rtype: tuple[basestring, list[list[basestring], basestring]
    """
    if not (record_path.endswith(RECORDING_EXT) and path.isfile(record_path)):
        return
    leaf_name = path.basename(record_path)

    root_path = path.dirname(record_path)
    leaves = []
    for _subitem in os.listdir(root_path):
        subfolder_path = path.join(root_path, _subitem)
        temp_leaves = []  # type: List[List[str]]
        if path.isdir(subfolder_path):

            sub_ret = get_paths_from(path.join(subfolder_path, leaf_name))
            if sub_ret:
                _, ret_sub_leaves, __ = sub_ret
            else:
                ret_sub_leaves = None
            if ret_sub_leaves is not None:
                if len(ret_sub_leaves) < 1:
                    temp_leaves.append([_subitem])
                else:
                    for ret_sub_leaf in ret_sub_leaves:
                        temp_leaves.append([_subitem] + ret_sub_leaf)
            leaves.extend(temp_leaves)
    return root_path, leaves, leaf_name


def get_paths_until(record_path):
    """
    Find all recordings with the same name in the path

    e.g

    Given /tmp/what/lol/rofl/lmao/v1.rec.py

    /tmp/what/
    └── lol
        ├── rofl
        │   ├── lmfao
        │   │   └── v1.rec.py
        │   └── v1.rec.py
        └── v1.rec.py

    It will find

    /tmp/what/lol/v1.rec.py
    /tmp/what/lol/rofl/v1.rec.py
    /tmp/what/lol/rofl/lmao/v1.rec.py

    /tmp/what/lol will be the root since it's the first directory with v1.rec.py

    TODO

    :param record_path:
    :type record_path: string
    :return: root_dir, dirs, recordname
    :rtype: tuple[basestring, list[list[basestring], basestring]
    """

    if not (record_path.endswith(RECORDING_EXT) and path.isfile(record_path)):
        return
    record_path = path.abspath(path.normpath(record_path))
    base_recording = path.basename(record_path)
    has_file_with_name = True

    root_dir = None
    current_path = path.dirname(record_path)
    dirs = [path.basename(current_path)]
    while has_file_with_name:
        current_path = path.dirname(current_path)
        recordings = get_recordings(current_path)
        has_file_with_name = base_recording in recordings
        if has_file_with_name:
            dirs.insert(0, path.basename(current_path))
        elif len(dirs) > 0:
            root_dir = path.join(current_path, dirs.pop(0))

    return root_dir, [dirs], base_recording
