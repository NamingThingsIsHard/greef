"""

"""

import pyautogui
from pyautogui import tweens

__author__ = "LoveIsGrief"


class Action(object):

    def execute(self, delay=0):
        raise NotImplementedError()

    def __repr__(self):
        args = ""
        if isinstance(self.__slots__, list):
            args = ",".join([
                "%(slot)s=%(value)s" % {
                    "slot": slot,
                    "value": repr(self.__getattribute__(slot))
                }
                for slot in self.__slots__
            ])

        return "%(classname)s(%(args)s)" % {
            "classname": self.__class__.__name__,
            "args": args
        }


DEFAULT_TWEEN = tweens.easeInOutCubic


class Click(Action):
    __slots__ = ["position", "button"]

    def __init__(self, position, button):
        self.position = position
        self.button = button

    @property
    def x(self):
        return self.position[0]

    @property
    def y(self):
        return self.position[1]

    def execute(self, delay=0):
        pyautogui.click(self.x, self.y, button=self.button, duration=delay, tween=DEFAULT_TWEEN)


class Drag(Action):
    __slots__ = ["start", "end", "button"]

    def __init__(self, start, end, button):
        self.start = start
        self.end = end
        self.button = button

    def execute(self, delay=0):
        move_duration = 0
        drag_duration = delay
        if delay > 0:
            move_duration = 0.3 * delay
            drag_duration = 0.7 * delay
        pyautogui.moveTo(
            self.start[0], self.start[1],
            duration=move_duration,
            tween=DEFAULT_TWEEN
        )
        pyautogui.dragTo(
            self.end[0], self.end[1],
            duration=drag_duration,
            tween=DEFAULT_TWEEN
        )


class Scroll(Action):
    __slots__ = ["value"]

    def __init__(self, value):
        self.value = value

    def execute(self, delay=0):
        pyautogui.scroll(self.value)


# Only export these symbols
__all__ = ["Action", "Click", "Drag", "Scroll"]
