import unittest
from os import path

from tree import get_paths_until, paths_have_file, get_paths_from

EXAMPLES_DIR = path.join(path.abspath(path.dirname(__file__)), "..", "..", "examples")
EXAMPLES_DIR = path.normpath(EXAMPLES_DIR)
TREES_DIR = path.join(path.abspath(path.dirname(__file__)), "data", "tree")


class PathsHaveFileTests(unittest.TestCase):

    def test_valid_dirs(self):
        expected_root = path.join(EXAMPLES_DIR, "xfce")

        self.assertTrue(paths_have_file(expected_root, [
            "open_settings", "open_appearance", "change_style"
        ], "v1.rec.py"))


class PathUntilTests(unittest.TestCase):
    def test_valid_recording_deep(self):
        expected_name = "v1.rec.py"
        root_dir, paths, name = get_paths_until(path.join(
            EXAMPLES_DIR, "xfce", "open_settings", "open_appearance",
            "change_style",
            expected_name
        ))
        self.assertEqual(name, expected_name)

        expected_root = path.join(EXAMPLES_DIR, "xfce")
        self.assertEqual(root_dir, expected_root)
        self.assertEqual(len(paths), 3)

        self.assertTrue(paths_have_file(expected_root, path, name))

    def test_valid_recording_one(self):
        expected_name = "v1.rec.py"
        root_dir, paths, name = get_paths_until(path.join(
            EXAMPLES_DIR, "xfce", expected_name
        ))

        self.assertEqual(name, expected_name)

        expected_root = path.join(EXAMPLES_DIR, "xfce")
        self.assertEqual(root_dir, expected_root)
        self.assertEqual(len(paths), 0)

        self.assertTrue(paths_have_file(expected_root, paths, name))


class PathsFromTests(unittest.TestCase):

    def test_just_root(self):
        abs_root, leaves, __ = get_paths_from(
            path.join(TREES_DIR, "just_root", "v1.rec.py"))
        self.assertEqual(abs_root, path.join(TREES_DIR, "just_root"))
        self.assertEqual(len(leaves), 0)

    def test_one_simple_sub(self):
        _, leaves, __ = get_paths_from(
            path.join(TREES_DIR, "one_simple_sub", "v1.rec.py"))
        self.assertEqual(len(leaves), 1)
        leaf_path = leaves[0]
        self.assertEqual(len(leaf_path), 1)

    def test_one_extended_sub(self):
        abs_root, leaves, __ = get_paths_from(
            path.join(TREES_DIR, "one_extended_sub", "v1.rec.py"))
        self.assertEqual(abs_root, path.join(TREES_DIR, "one_extended_sub"))
        self.assertEqual(len(leaves), 1)
        leaf_path = leaves[0]
        self.assertEqual(len(leaf_path), 3)

    def test_two_simple_subs(self):
        _, leaves, __ = get_paths_from(
            path.join(TREES_DIR, "two_simple_subs", "v1.rec.py"))
        self.assertEqual(len(leaves), 2)
        for leaf_path in leaves:
            self.assertEqual(len(leaf_path), 1)

    def test_two_extended_subs(self):
        _, leaves, __ = get_paths_from(
            path.join(TREES_DIR, "two_extended_subs", "v1.rec.py"))
        self.assertEqual(len(leaves), 2)
        for leaf_path in leaves:
            self.assertEqual(len(leaf_path), 3)


class PathsHaveFileTest(unittest.TestCase):

    def test_no_file(self):
        self.assertRaises(ValueError, paths_have_file, "/", ["tmp"], "idontexist")

    def test_has_files(self):
        self.assertTrue(paths_have_file(TREES_DIR, [
            "one_extended_sub",
            "sub",
            "ext_sub",
            "ext_ext_sub",
        ], "v1.rec.py"))


if __name__ == '__main__':
    unittest.main()
