echo "Updating apt repos"
while ! apt-get update ; do
    sleep 1
done

echo "Installing deps"
apt-get install -y -q \
    python3 \
    python3-dev \
    python3-xlib \
    scrot \
    python3-tk \
    linux-headers-generic \
    curl \
    gcc

echo "Install an up to date pip"
curl -s "https://bootstrap.pypa.io/get-pip.py" | python3
